module One exposing (..)

import Sprite exposing (..)
import Array
import Html exposing (..)
import Html.Attributes as A
import Html.App exposing (..)
import Time exposing (Time, every, millisecond)


type Msg
    = Tick Time


type alias Model =
    Sprite { left : Int }


init : Model
init =
    { sheet = "https://10firstgames.files.wordpress.com/2012/02/actionstashhd.png"
    , rows = 16
    , columns = 16
    , size = ( 2048, 2048 )
    , frame = 0
    , dope = dopeRow 1 |> Array.fromList
    , left = 0
    }


dopeRow : Int -> List ( Int, Int )
dopeRow y =
    List.map (\x -> ( x, y )) [0..15]


view : Model -> Html Msg
view s =
    div
        []
        [ node
            "sprite"
            [ A.style <|
                sprite s
                    ++ [ ( "position", "absolute" )
                       , ( "left", toString s.left ++ "px" )
                       ]
            ]
            []
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update _ s =
    ( advance s
        |> (\s' -> { s' | left = s.left + 4 })
    , Cmd.none
    )


main : Program Never
main =
    program
        { init = ( init, Cmd.none )
        , update = update
        , view = view
        , subscriptions = always <| every (millisecond * 33) Tick
        }
